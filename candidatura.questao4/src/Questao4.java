import java.util.Random;
public class Questao4 {

    private static void diasMes (int m){
        switch (m){
            case 2:
                System.out.println("Mês com menos de 30 dias");
                break;
            case 4:
                System.out.println("Mês com 30 dias");
                break;
            case 6:
                System.out.printf("Mês com 30 dias");
                break;
            case 9:
                System.out.println("Mês com 30 dias");;
                break;
            case 11:
                System.out.println("Mês com 30 dias");
                break;
            default:
                System.out.println("Mês com 31 dias");
        }
    }

    public static void qualDiaMes(){
        Random random = new Random();
        int mes = random.nextInt(12) + 1;
        System.out.println("Número do Mês: " + mes);
        diasMes(mes);
    }

    public static void main (String [] args){
        qualDiaMes();
    }
}
