import java.util.Random;
public class Questao2 {
    public static void main (String [] args){

        Random random = new Random();
        int r = random.nextInt(10) + 1;
        int s = random.nextInt(100) + 1;

        System.out.println("Valor S: " + s);
        System.out.println("Valor R: " + r);

        for (int i = 1; i <= s; i++){
            if (i % (r*2) == 0){
                System.out.println("Multiplo do dobro de R encontrado");
                break;
            }
            if (i%2==0) System.out.println(i);
        }
    }
}
