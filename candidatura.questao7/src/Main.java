public class Main {
    public static void executar(String letter){
        letter.toLowerCase();
        switch (letter){
            case "a":
                A contratoA = new A();
                contratoA.imprima();
                break;
            case "b":
                B contratoB = new B();
                contratoB.imprima();
                break;
            default:
                C contratoC = new C();
                contratoC.imprima();
        }
    }
    public static void main (String [] args){
        executar("a");
    }
}
