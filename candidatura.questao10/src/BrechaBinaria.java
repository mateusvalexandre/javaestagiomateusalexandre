public class BrechaBinaria {
    public static int solucao(int valorDecimal){
        String binario = "";
        int N = 0;
        int nMax = 0;

        while (true){
            int resto = valorDecimal%2;
            binario += String.valueOf(resto);
            valorDecimal /= 2;
            if (valorDecimal < 1) break;
        }

        for (int i = binario.length() -2; i >= 0; i--){
            char elemento = binario.charAt(i);
            if (elemento != '0'){
                if (N >= nMax)nMax = N;
                N = 0;
            } else N++;
        }
        return nMax;
    }

    public static void main (String [] args){
        int brechaBinaria = solucao(1041);
        System.out.println(brechaBinaria);
    }
}
