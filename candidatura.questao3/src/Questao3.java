import java.util.Random;
public class Questao3 {
    public static void main (String [] args){

        Random random = new Random();
        int r = random.nextInt(10) + 1;
        int s = random.nextInt(100) + 1;

        System.out.println("Valor S: " + s);
        System.out.println("Valor R: " + r);

        int soma = 0;

        for (int i = 1; i <= s; i++){
            if (i % r == 0){
                System.out.println(i);
                soma += i;
            }
        }
        System.out.println("A soma dos divisores de R existentes entre 1 e S é: " + soma);
    }
}
